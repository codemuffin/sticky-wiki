import { setInputsFromOptions } from './setInputsFromOptions.js';
import { submitBtnListener } from './submitBtnListener.js';
import { optionsGet } from '../utils/storage/optionsGet.js';
import { OPTS_KEYS, KEYS, OPTS_DEFAULTS, DEBUG } from '../shared/settings.js';
import { chromeStorageGet } from '../utils/storage/chromeStorageGet.js';

/**
 * Main init for options
 *
 * @async
 *
 * @return {void}
 */
export async function initOptions()
{
	const userOpts = await optionsGet( OPTS_KEYS, KEYS.localStorageKey, OPTS_DEFAULTS, false, DEBUG );

	console.log( '[initOptions]', userOpts);

	setInputsFromOptions( userOpts );
	submitBtnListener( DEBUG );
}




// const userOptions = await chromeStorageGet( OPTS_KEYS, true );

