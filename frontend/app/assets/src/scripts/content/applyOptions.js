import { objectHasProp } from './../utils/object/objectHasProp.js';

/**
 * Set body classes by user options
 *
 * @return {void}
 */
export function applyOptions( options, debug = false )
{
	for ( const optionKey in options )
	{
		if ( objectHasProp( options, optionKey ) )
		{
			const optionEnabled = options[optionKey];

			if ( debug )
			{
				console.log( `[StickyWiki] Option "${optionKey}" = `, optionEnabled );
			}

			if ( typeof optionEnabled !== 'boolean' )
			{
				console.error( `[Sticky Wiki] applyOptions: Expected boolean option value for "${optionKey}", but type was ${typeof optionEnabled}, value was:`, optionEnabled );

				continue;
			}

			// Set data attribute: data-sw-*
			document.body.setAttribute( `data-sw-${optionKey}`, optionEnabled );
		}
	}
}
