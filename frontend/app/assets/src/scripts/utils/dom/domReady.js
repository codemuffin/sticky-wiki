
export function domReady( callbackFunc )
{
	if ( document.readyState !== 'loading' )
	{
		// Page has already loaded
		callbackFunc();
	}
	else if ( document.addEventListener )
	{
		// Modern browsers
		// DOMContentLoaded = HTML has loaded, DOM is built, media might not be loaded
		document.addEventListener( 'DOMContentLoaded', callbackFunc );

		// @TODO: Add option to use load:
		// load = Everything has loaded, media included
	}
	else
	{
		// IE support, not needed for extensions but good to keep
		document.attachEvent( 'onreadystatechange', function()
		{
			if ( document.readyState === 'complete' )
			{
				callbackFunc();
			}
		});
	}
}