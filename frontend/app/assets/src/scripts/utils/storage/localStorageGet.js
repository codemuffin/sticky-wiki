
/**
 * Get data from localstorage
 *
 * @param   {string}   key           Data key
 * @param   {boolean}  parseJSON     True to parse the retreived data (if the parse fails, returns a rejected promise with error object)
 * @param   {boolean}  rejectOnFail  True to return a rejected promise if the data is empty or absent (if false and data is empty, resolves with null)
 * @param   {boolean}  debug         True to log dev info
 *
 * @return  {Promise}  Resolves with retreived data. If data is empty, either resolves with null (if rejectOnFail=false) or rejects (if rejectOnFail=true)
 */
export async function localStorageGet( key, parseJSON = false, rejectOnFail = false, debug = false )
{
	const result = localStorage.getItem( key );

	if ( result !== null && typeof result === 'string' && result.length > 0 )
	{
		if ( debug )
		{
			console.log( `[localStorageGet] Retreived data for "${key}":`, result );
		}

		if ( parseJSON )
		{
			try
			{
				const resultParsed = JSON.parse( result );

				if ( debug )
				{
					console.log( `[localStorageGet] JSON parse successful for "${key}":`, resultParsed );
				}

				return Promise.resolve( resultParsed );
			}
			catch ( err )
			{
				if ( debug )
				{
					console.error( `[localStorageGet] JSON parse failed for "${key}"`, err );
				}

				if ( rejectOnFail )
				{
					return Promise.reject( err );
				}
			}
		}
		else
		{
			return Promise.resolve( result );
		}
	}
	else
	{
		if ( rejectOnFail )
		{
			return Promise.reject( 'Data was empty' );
		}
		else
		{
			return Promise.resolve( null );
		}
	}
}
