
/**
 * Check if an object has the supplied property, via hasOwnProperty
 *
 * @param   {object}  obj   Object to check
 * @param   {string}  prop  Property to check for
 *
 * @return  {bool|null}     True if it contains the property, false if not, null if no object or property string were provided
 */
export function objectHasProp( obj, prop )
{
	if ( typeof obj !== 'object' || typeof prop !== 'string' || prop.length === 0 )
	{
		return null;
	}

	return ( Object.prototype.hasOwnProperty.call( obj, prop ) );
}
