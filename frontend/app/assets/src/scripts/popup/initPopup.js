
/**
 * Main init for popup.
 * Runs from within the extension popup
 */
export function initPopup()
{
	// Options button
	document.querySelector( '.options' ).addEventListener( 'click', () => chrome.runtime.openOptionsPage() );
}
