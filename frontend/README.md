# Frontend Boilerplate 1.0.0

Quick start for FED with minimal config.

## Install

`npm install`

## Build

Compile JS & CSS assets, then copy them to the production directory.

`npm run build`

Compile assets with:

- `compile:all`
- `compile:scripts`
- `compile:scripts:unmin`
- `compile:styles`

Copy assets with:

- `copy:all`
- `copy:scripts`
- `copy:styles`
- `copy:vendor`

## Watch

Watch src assets. Recompiles them when edited, then copies them to the production directory.

`npm run watch`

## Vendor

Vendor assets are not included in the watch task. Copy them after edits with either:

- `build`
- `copy:all`
- `copy:vendor`
