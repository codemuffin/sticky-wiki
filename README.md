# Sticky Wiki (Improved Wikipedia)

Makes Wikipedia TOCs into a sticky sidebar.

Also centers text on wide screens, ads better responsive support, and other fixes. Has an options page to configure to your liking.

**Install the extension:**

https://chrome.google.com/webstore/detail/sticky-wiki-improved-wiki/ghnnofboenhhbhkfcfcjnfdeehdebnin


## Setup

    cd frontend
    npm i

## Build

Compile assets and copy them to `./extension`

    npm run build

## Watch

Start Browser Sync and watches for file changes. Runs relevant build task(s) on file change.

    npm run start

## Single Tasks

See `./frontend/package.json` for dedicated tasks
