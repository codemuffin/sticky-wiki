import { KEYS, OPTS_DEFAULTS, OPTS_KEYS } from '../shared/settings.js';
import { domReady } from '../utils/dom/domReady.js';
import { optionsGet } from '../utils/storage/optionsGet.js';
import { applyOptions } from './applyOptions.js';
import { sKeyPressListener } from './sKeyPressListener.js';

/**
 * Main init for options
 *
 * @return {void}
 */
export async function initContent()
{
	const userOpts = await optionsGet( OPTS_KEYS, KEYS.localStorageKey, OPTS_DEFAULTS );

	console.log({ userOpts });

	domReady( () => {
		applyOptions( userOpts, false );
		sKeyPressListener( userOpts );
	});
}
