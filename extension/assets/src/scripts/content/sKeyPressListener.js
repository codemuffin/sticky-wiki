import { inputHasFocus } from '../utils/element/inputHasFocus.js';

/**
 * Listen for keyPressListener
 *
 * [S]: Toggle sidebar, does nothing if an input is focused
 *
 * [SHIFT] + [S]: Focus search, same as shift+alt+f but less awkward
 *
 * @return {void}
 */
export function sKeyPressListener( options = {} )
{
	const enableToggle = options['ks-toggle-toc'];
	const enableFocus  = options['ks-focus-search'];

	if ( !enableToggle && !enableFocus )
	{
		return;
	}

	document.addEventListener( 'keyup', e =>
	{
		if ( inputHasFocus() )
		{
			return;
		}

		if ( e.key === 's' || e.key === 'S' )
		{
			if ( enableFocus && ( e.shiftKey === true ) )
			{
				// SHIFT + S = Focus search
				document.getElementById( 'searchInput' ).focus();
			}
			else if ( enableToggle )
			{
				// S = Toggle sidebar (triggers TOC toggle click)
				document.getElementById( 'toctogglecheckbox' ).click();
			}
		}
	});
}
