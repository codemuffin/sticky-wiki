
/**
 * Set checkboxes by previous user options.
 * Triggered after user options are retrieved
 *
 * @return {void}
 */
export function setInputsFromOptions( opts, debug = false )
{
	document.querySelectorAll( '.js-checkbox-input' ).forEach( cb =>
	{
		const optionKey = cb.getAttribute( 'name' );

		if ( Object.prototype.hasOwnProperty.call( opts, optionKey ) )
		{
			const optionEnabled = opts[optionKey];

			// protect against corrupted data
			if ( typeof optionEnabled === 'boolean' )
			{
				cb.checked = optionEnabled;

				if ( debug )
				{
					console.log( `[setInputsFromOptions] ${optionKey}:`, optionEnabled );
				}
			}
			else
			{
				if ( typeof optionValue !== 'boolean' )
				{
					console.error( `[setInputsFromOptions] Expected boolean option value for "${optionKey}", but type was ${typeof optionEnabled}, value was:`, optionEnabled );

					return;
				}
			}
		}
		else
		{
			if ( debug )
			{
				console.log( `[setInputsFromOptions] UNKNOWN OPTION: ${optionKey}:` );
			}
		}
	});
}
