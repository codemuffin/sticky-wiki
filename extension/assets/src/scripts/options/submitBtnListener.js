// import { DEBUG, OPTIONS } from './../shared/settings';
import { optionsSetOLD } from './../utils/storage/optionsSetOLD.js';
import { optionsGet } from '../utils/storage/optionsGet.js';
import { OPTS_KEYS, KEYS, OPTS_DEFAULTS } from './../shared/settings.js';

/**
 * Listen for submit button interaction, saves options on click
 *
 * @return {void}
 */
export async function submitBtnListener( debug = false )
{
	let lastUSerOpts = await optionsGet( OPTS_KEYS, KEYS.localStorageKey, OPTS_DEFAULTS );
	let newOptions = {};

	document.querySelector( '.js-submit-btn' ).addEventListener( 'click', async () =>
	{
		// const lastOptions = {};


		// Get user config
		document.querySelectorAll( '.js-checkbox-input' ).forEach( cb =>
		{
			const optName = cb.getAttribute( 'name' );
			const optEnabled = cb.checked;

			if ( debug )
			{
				console.log( '[submitBtnListener] checkbox:', optName, optEnabled );
			}

			newOptions[optName] = optEnabled;
		});

		newOptions = Object.assign( lastUSerOpts, newOptions );

		optionsSetOLD( newOptions, debug );
	});
}
