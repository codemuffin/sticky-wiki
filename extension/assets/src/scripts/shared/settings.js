

// Extension Config
// ============================================================================

export const DEBUG = true;

export const KEYS = {
	'localStorageKey': 'sw-ext-options',
};

export const OPTIONS = {
	'sticky-toc':           true,
	'centered-content':     true,
	'visual-declutter':     true,
	'fix-table-align':      true,
	'fix-small-responsive': true,
	'ks-toggle-toc':        true,
	'ks-focus-search':      true,

	// Hide features
	'hide-user':            false,
	'hide-indicators':      false,
	'hide-site-notices':    false,
};


export const OPTS_EXT = {
	'sticky-toc':            { type: 'boolean', default: true },
	'centered-content':      { type: 'boolean', default: true },
	'visual-declutter':      { type: 'boolean', default: true },
	'fix-table-align':       { type: 'boolean', default: true },
	'fix-small-responsive':  { type: 'boolean', default: true },
	'ks-toggle-toc':         { type: 'boolean', default: true },
	'ks-focus-search':       { type: 'boolean', default: true },

	// Hide features
	'hide-user':             { type: 'boolean', default: false },
	'hide-indicators':       { type: 'boolean', default: false },
	'hide-site-notices':     { type: 'boolean', default: false },
	'hide-donate-nag':       { type: 'boolean', default: false },
};


// Generated
// ============================================================================

export const OPTS_KEYS = Object.keys( OPTS_EXT );


// Defaults

let defaults = {};

OPTS_KEYS.forEach( optKey => defaults[optKey] = OPTS_EXT[optKey].default );

export const OPTS_DEFAULTS = defaults;
