
/**
 * Check if the active element is type of input
 *
 * Input types are: input, textarea, and select
 *
 * @return  {Boolean}  True if the active element is an input, false otherwise
 */
export function inputHasFocus()
{
	const inputTagsArr = [ 'input', 'textarea', 'select' ];
	const activeElTag = document.activeElement.tagName.toLowerCase();

	return inputTagsArr.includes( activeElTag );
}
