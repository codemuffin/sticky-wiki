import { chromeStorageAvailable } from './chromeStorageAvailable.js';

/**
 * Retreive data from Chrome's storage
 *
 * @async
 *
 * @param   {mixed}    keys           Single key string | Array of key strings | Object containing keys to retreive | null to get everything
 * @param   {boolean}  [debug=false]  True to log results
 *
 * @return  {Promise}  Resolves with retreived data. Rejects if chrome storage is not available
 */
export async function chromeStorageGet( keys, debug = false )
{
	return new Promise( ( resolve, reject ) =>
	{
		if ( chromeStorageAvailable() )
		{
			chrome.storage.sync.get( keys, ( syncResult ) =>
			{
				if ( debug )
				{
					console.log( '[getStorageAsync] chrome.storage:', syncResult );
				}

				resolve( syncResult );
			});
		}
		else
		{
			reject( '[chromeStorageGet] chrome.storage is not available' );
		}
	});
}
