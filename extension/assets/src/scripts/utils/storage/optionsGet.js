import { chromeStorageGet } from './chromeStorageGet.js';
import { chromeStorageAvailable } from './chromeStorageAvailable.js';
import { localStorageGet } from './localStorageGet.js';

/**
 * Get user options from Chrome's extension storage, merged with the default
 * options.
 *
 * Falls back to localstorage if the storage APi is not available
 *
 * @async
 *
 * @param {object}   keys                   Option keys
 * @param {string}   localStorageKey        Data key for localStorage fallback
 * @param {object}   defaults               Default options data. Merged with retrieved user options
 * @param {boolean}  [rejectOnEmpty=false]  True to return a rejected promise if there is no user data available
 * @param {boolean}  [debug=false]          True to log debug info
 *
 * @return {Promise} Resolved promise with user options, merged with defaults. Rejected promsie if rejectOnEmpty=true and no user data was available
 */
export async function optionsGet( keys, localStorageKey, defaults, rejectOnEmpty = false, debug = false )
{
	let userOptions = {};

	try
	{
		if ( chromeStorageAvailable() )
		{
			userOptions = await chromeStorageGet( keys, debug );

			if ( debug )
			{
				console.log( '[optionsGet] Success, got from chrome.storage:', userOptions );
			}

			const mergedOpts = Object.assign( defaults, userOptions);
			return Promise.resolve( mergedOpts );
		}
		else
		{
			userOptions = await localStorageGet( localStorageKey, true, true, debug );

			if ( debug )
			{
				console.log( '[optionsGet] Success, got from localStorage:', userOptions );
			}

			const mergedOpts = Object.assign( defaults, userOptions);
			
			return Promise.resolve( mergedOpts );
		}
	}
	catch( err )
	{
		if ( rejectOnEmpty )
		{
			console.warn( '[optionsGet] Failed to retreive extension options', err );

			return Promise.reject( err );
		}
	}
}
