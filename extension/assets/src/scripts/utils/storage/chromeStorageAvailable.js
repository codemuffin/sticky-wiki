
/**
 * Check if chrome.storage is available
 * @TODO: This is repeated in main and options
 *
 * @return  {Boolean}  True if chrome.storage is available, false otherwise
 */
export function chromeStorageAvailable()
{
	return ( typeof chrome === 'object' && Object.prototype.hasOwnProperty.call( chrome, 'storage' ) )
}
