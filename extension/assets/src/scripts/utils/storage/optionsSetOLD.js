import { chromeStorageAvailable } from './chromeStorageAvailable.js';
import { KEYS } from '../../shared/settings.js';

/**
 * Save options to Chrome's extension storage.
 * Falls back to localstorage if the storage APi is not available
 *
 * @return {void}
 */
export function optionsSetOLD( options, debug = false )
{
	if ( chromeStorageAvailable() )
	{
		// Use Chrome extension storage API
		chrome.storage.sync.set( options, () =>
		{
			if ( debug )
			{
				console.log( '[optionsSetOLD] Options saved (chrome.storage)', options );
			}
		});
	}
	else
	{
		// Use localstorage fallback
		localStorage.setItem( KEYS.localStorageKey, JSON.stringify( options ) );

		if ( debug )
		{
			console.log( '[optionsSetOLD] Options saved (localStorage)', options );
		}
	}
}
