{
  "version": 3,
  "sources": [
    "node_modules\\browser-pack-flat\\_prelude",
    "app/assets/src/scripts/popup/initPopup.js",
    "app/assets/src/scripts/popup.js"
  ],
  "names": [
    "_$initPopup_2",
    "document",
    "querySelector",
    "addEventListener",
    "chrome",
    "runtime",
    "openOptionsPage",
    "initPopup"
  ],
  "mappings": "CAAA,WACA,IAAAA,EAAA,gECIO,WAGNC,SAASC,cAAe,YAAaC,iBAAkB,SAAS,WAAA,OAAMC,OAAOC,QAAQC,yBCRtFN,EAEAO,aFFA",
  "sourcesContent": [
    "(function(){\n",
    "\r\n/**\r\n * Main init for popup.\r\n * Runs from within the extension popup\r\n */\r\nexport function initPopup()\r\n{\r\n\t// Options button\r\n\tdocument.querySelector( '.options' ).addEventListener( 'click', () => chrome.runtime.openOptionsPage() );\r\n}\r\n",
    "import { initPopup } from './popup/initPopup';\r\n\r\ninitPopup();\r\n"
  ],
  "sourceRoot": ""
}